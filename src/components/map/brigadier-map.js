import React, { Component } from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import { getWorkerEvents } from '../../api/api';
import "./brigadier-map.css"

export default class BrigadierMap extends Component {
  state = {
    lat: 41.6836054,
    lng: 44.9106029,
    zoom: 13,
    markers: []
  }

  componentDidMount() {
    this.handleObjectsBind();
  }

  handleObjectsBind = async () => {
    const returnedData = await getWorkerEvents();
    this.setState({ markers: returnedData })
  }
  render() {
    const markers = this.state.markers;
    const position = [this.state.lat, this.state.lng]
    return (
      <div>
        <Map className="mapDiv" center={position} zoom={this.state.zoom}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {
            markers.map(element =>
              <div>
                
              <Marker position={[element.latitude, element.longitude]}>
                  <Popup>
                    <ul>
                      <li>{element.author}</li>
                      <li>{element.employee.firstName} {element.employee.lastName}</li>
                      <li>{element.employee.privateNumber}</li>
                    </ul>
                </Popup>
                </Marker>
              </div>
              
            )
          }
        </Map>
      </div>
    )
  }
}