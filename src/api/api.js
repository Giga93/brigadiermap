import axios from 'axios'

const url = 'http://192.168.22.12:8585'

export const getWorkerEvents = async () => {
    try{
        const startDate="2020-08-01";
        const endDate="2020-08-30";

        return await axios.get(`${url}/api/Events/worker?startDate=${startDate}&endDate=${endDate}`)
            .then(res => res.data);
    }catch(err){throw err}
}