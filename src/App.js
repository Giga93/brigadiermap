import React from 'react';
import BrigadierMap from '../src/components/map/brigadier-map';
import Header from '../src/components/header/header';
import Sidebar from './components/sidebar/sidebar';
import './App.css';
import '../src/components/sidebar/sidebar.css'


function App() {
  return (
    <div className="App">
      <Header />
      <Sidebar />
      <BrigadierMap />
    </div>
  );
}

export default App;
